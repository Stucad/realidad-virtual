﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.MenuPrincipal.Scripts
{
	public class MenuPrincipal : MonoBehaviour {

		public void Quit()
		{
			Application.Quit();
		}

		public void Iniciar(int nivel)
		{ 
			DataController dataController = new DataController ();
			dataController.SaveData();
		    SceneManager.LoadScene(nivel); 
		}
	}
}
