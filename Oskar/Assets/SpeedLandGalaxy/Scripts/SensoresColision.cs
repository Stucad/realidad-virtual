﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensoresColision : MonoBehaviour {

	private ControladorDeVueltas controladorDeVueltas;

	void OnTriggerExit(Collider collider)
	{  
		if (collider.name == "Sensor") { 
			controladorDeVueltas = collider.transform.GetComponentInParent<ControladorDeVueltas>(); 
			controladorDeVueltas.ControlarVuelta(this.gameObject);
		}
	}
}
