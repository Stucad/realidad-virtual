﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class CapturarRecolectores : MonoBehaviour {

		public float puntosPorRecolector = 0.25f;
		public Text nivelCarro;
		public Image imgIndicadorPuntos;
		public int nivel;// Cuenta los niveles 
		public int[] CantidadRecolectoresTipo; 

		private int cantidadRecolectores;  
		private string[,] recolectoresSecundariosAsignados = null;
		private GameObject[] recolectoresSecundarios;
		private int fila = -1, columna = 1; 
		private GameObject cantidadPorTipoActual; 
		private GameObject tiposDeSenalesTransitoActual;
		private string[] tipoRecolector; 
		private GameObject controladorJuegoCarrera;
 
		/// <summary>
		/// Start this instance.
		/// </summary>
		void Start()
		{   
			cantidadPorTipoActual = IndicadoresDelJuego.cantidadPorTipoActual;
		    tipoRecolector = VariablesGlobables.recolectablePrincipal.GetComponent<ControladorRecolectores>().tipoRecolector;//Se entrega el recolector principal donde contiene todas los recolectores
			tiposDeSenalesTransitoActual = IndicadoresDelJuego.tiposDeSenalesTransitoActual;
		    recolectoresSecundarios = VariablesGlobables.recolectoresSecundarios; 
			recolectoresSecundariosAsignados = new string[recolectoresSecundarios.Length,2];
			CantidadRecolectoresTipo = new int[recolectoresSecundarios.Length]; 
			RecorrerRecolectores(); 
		} 

		/// <summary>
		/// Recorre los recolectores y les asigna nombre y false que indica que aún no los ha colisionado por vueta
		/// </summary>
		public void RecorrerRecolectores()
		{
			for (int i = 0; i < recolectoresSecundarios.Length; i++) { 
					recolectoresSecundariosAsignados[i,0] = recolectoresSecundarios[i].name;  
					recolectoresSecundariosAsignados[i,columna] = "false";
				}
		} 

	/// <summary>
	///  Crea la sensación de capturar el recolector y impide que lo vuelva hacer
	/// con su grupo
	/// </summary>
	/// <param name="nombreRecolectorAgrupado">Nombre recolector agrupado.</param>
	public void IdenficarGruposRecolectores(GameObject recolector)
	{ 
		string nombreRecolectorPadre = recolector.transform.parent.name; // captura objetos que estan nombrados en este orden así: signos 1, 2 ... 
		for (int i = 0; i < recolectoresSecundarios.Length; i++) {   
			if (recolectoresSecundariosAsignados[i, 0] == nombreRecolectorPadre && 
				recolectoresSecundariosAsignados[i, columna] == "false") { 
				fila = i;  //Cuando es exito en OnTriggerExit
				i = recolectoresSecundarios.Length; 
			} 
		} 
	}
  
  
	/// <summary>
	///   Asigna true que fue capturado el recolector 
	/// </summary> 
	public void AsignarGruposRecolectores(GameObject recolector)
	{   
		if (recolectoresSecundariosAsignados[fila, columna] == "false" && recolector.gameObject.activeSelf == true) {//Asigna que el recolector ya lo colisiono
			recolectoresSecundariosAsignados[fila, columna] = "true"; 
			if (transform.tag == "Player") { 
				AumentarPuntuacion();
				SubirNivel();
				int index = recolector.transform.parent.transform.Find(recolector.gameObject.name).transform.GetSiblingIndex(); // Entrega en que posición esta el recolector
				ContarRecolectoresPorTipo(index, recolector.gameObject.name);
			} 
			recolector.gameObject.SetActive(false);  
		}
	}

		/// <summary>
		/// Contars the recolectores por tipo. cuenta recolector por tipo
		/// </summary>
		/// <param name="index">Index.</param>
		/// <param name="nombre">Nombre.</param>
		void ContarRecolectoresPorTipo(int index, string nombre)
		{ 
			string nombreTipoRecolector; 
			int contar = CantidadRecolectoresTipo.Length > 0 ?  CantidadRecolectoresTipo[index] + 1 : 1;  
			CantidadRecolectoresTipo[index] = contar;
			nombreTipoRecolector = tipoRecolector[index];
				for (int i = 0; i < cantidadPorTipoActual.transform.childCount; i++) {
					if (tiposDeSenalesTransitoActual.gameObject.transform.GetChild(i).name == nombreTipoRecolector) {
						int cantidadActual = cantidadPorTipoActual.gameObject.transform.GetChild(i).GetComponentInChildren<Text>().text.Length > 0?
							int.Parse (cantidadPorTipoActual.gameObject.transform.GetChild(i).GetComponentInChildren<Text>().text) + 1:1; 
							cantidadPorTipoActual.gameObject.transform.GetChild(i).GetComponentInChildren<Text>().text = cantidadActual.ToString();
							i = cantidadPorTipoActual.transform.childCount;
				     } 
				}
		}

		void AumentarPuntuacion(){
			cantidadRecolectores++;
		}

		/// <summary>
		/// Subirs the nivel. sube el nivel de cada recolectable
		/// </summary>
		void SubirNivel(){ 
			imgIndicadorPuntos.fillAmount = cantidadRecolectores * puntosPorRecolector;
			if (imgIndicadorPuntos.fillAmount == 1f) {
				nivel = nivel + 1;
				nivelCarro.text = nivel.ToString();
				LimpiarIndicadorPuntos();
			}
		}

		/// <summary>
		/// Limpiars the indicador puntos. cada vez que sube nivel limpia la imagen
		/// </summary>
		public void LimpiarIndicadorPuntos()
		{  
			imgIndicadorPuntos.fillAmount = 0.0f;
		}
	}  