﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityStandardAssets.Vehicles.Car; 
  
public class ControladorDeVueltas : MonoBehaviour {
   
	    public int VueltasLlevadas;
		public int SensoresDisparados=0;
		public bool[] sensoresVueltas;  
	    public bool unaVezEntra= false; // Ojo corregir esto 
		private int indicesContados;
		private bool desacelerar= false, parar= false;  
	    private CarController carController;
	    private GameObject sensores;
	    private Transform rootCar;
 
		/// <summary>
		/// Awake this instance.
		/// </summary>
		private void Awake()
		{  
		  rootCar = this.transform;
		  carController = rootCar.GetComponent<CarController>();
		}

		private void Start()
		{
			sensores= VariablesGlobables.sensores;
			indicesContados= sensores.transform.childCount;
			sensoresVueltas= new bool[indicesContados];
		}
 
		/// <summary>
		/// Update this instance.
		/// </summary>
		void Update(){ 
			if (desacelerar == true && parar == false) {// cuando cumple todas las vueltas
				carController.Move (0.0f, 0.0f, 1.0f, 1.0f);
				if (System.Math.Round(carController.CurrentSpeed,2) < 1.0f) { 
					carController.Move(0.0f, 0.0f, 0.0f, 0.0f);
					carController.enabled = false; 
					parar = true;
				} 
			} 
		}
 
	public void ControlarVuelta(GameObject collider)
	{
		int index = sensores.transform.Find(collider.gameObject.name).transform.GetSiblingIndex(); 
		if (index == 0) {
			if (sensoresVueltas[index] == false) { // cuando entra por primera vez 
				sensoresVueltas[index] = true;
				DispararSensorPositivo();
			} else if(SensoresDisparados > 0){
				sensoresVueltas[index] = false;
				DispararSensorNegativo();
			}  
		} else if (index == indicesContados - 1) {// cuando ha logrado alcanzar el maximo de sensores pasados por el carro
			if (sensoresVueltas [index - 1] == true) { 
				Vueltas();
				sensoresVueltas [index - 1] = false; 
				VariablesGlobables.recolectablePrincipal.GetComponent<ControladorRecolectores>().ActivarRecolectoresTransito(); // vuelve aparecer los recolectores
				rootCar.GetComponent<CapturarRecolectores>().RecorrerRecolectores(); 
				ReiniciarSensoresDisparados();
				FinalizarCarrera();
			}   
		} else { // cuando entra por x veces...
			if (sensoresVueltas[index - 1] == true) {   
				sensoresVueltas[index] = true;
				sensoresVueltas[index - 1] = false;
				DispararSensorPositivo();
			} else if(SensoresDisparados > 0){ 
				sensoresVueltas[index - 1] = true;
				sensoresVueltas[index] = false;
				DispararSensorNegativo();
			} 
		} 
	}  
 
		//Para el carro y entrega los datos al menu controlador de juego carrera
		public void FinalizarCarrera()
		{ 
		     int numeroVueltas = VariablesGlobables.numeroVueltas; 
			if (VueltasLlevadas == numeroVueltas ) {  // && finalizoCarrera == false
			if (this.gameObject.tag == "Player") { 
				ControlUsuarioCarro controlUsuarioCarro = rootCar.GetComponent<ControlUsuarioCarro>();
					controlUsuarioCarro.enabled = false; 
				Turbo Turbocarro = rootCar.GetComponent<Turbo>(); // se debe sacar fuera del if cuando se mejore el turbo para la IA, se aplica para play y IA 
					Turbocarro.TurboCar(0f);
					Turbocarro.enabled = false; 
				} else {
				CarAIControl carAIControl = this.transform.GetComponentInParent<UnityStandardAssets.Vehicles.Car.CarAIControl>();
					carAIControl.enabled = false;
				}
			  int posicion = rootCar.GetComponentInChildren<EmitirRayo>().PosicionJugador;
			   FinalCarrera(posicion, this.name, rootCar.GetComponent<CapturarRecolectores>().CantidadRecolectoresTipo, this.tag); //entrega la posicion y el nombre
				desacelerar = true;
			    rootCar.GetChild(1).gameObject.SetActive(false); // desactiva los sensores de posinamiento  
			}   
		}
 
			/// <summary>
			/// Finals the carrera. Este metodo es invocado por el controlador de carreras para capturar los tiempos de cada jugador
			/// </summary>
			/// <param name="posicion">Posicion.</param>
			/// <param name="nombre">Nombre.</param>
			/// <param name="CantidadRecolectoresTipo">Cantidad recolectores tipo.</param>
			public static void FinalCarrera(int posicion, string nombre, int[] CantidadRecolectoresTipo, string Jugador)
			{
				if (Jugador == "Player") {
			          VariablesGlobables.pararTiempo = true; 
				}
		        IndicadoresDelJuego.EntregarIndicadoresFinales(posicion, CantidadRecolectoresTipo, Jugador, nombre); 
			}

		private void ActivarSensores(int i)
		{
			sensores.gameObject.transform.GetChild(i).gameObject.SetActive(true);
		}
 
		/// <summary>
		/// Oculta los sensores al dar la vuelta
		/// </summary>
		private void DesactivarSensores()
		{ 
			for (int i = 1; i < indicesContados - 1; i++) { 
				sensores.gameObject.transform.GetChild(i).gameObject.SetActive(false); 
			} 
		}
 
		/// <summary>
		/// Cuenta las vueltas para pararlo
		/// </summary>
		private void Vueltas()
		{
			VueltasLlevadas++; 
		}

		/// <summary>
		/// Disparars the sensor. Cuando el carro va en dirección correcta
		/// </summary>
		private void DispararSensorPositivo()
		{
			SensoresDisparados++;
		}

		/// <summary>
		/// Disparars the sensor negativo. Cuando el carro va en dirección contraria
		/// </summary>
		private void DispararSensorNegativo()
		{
			SensoresDisparados--;
		}

		/// <summary>
		/// Limpiars the sensores disparados. Cuando inicia una nueva vuelta
		/// </summary>
		private void ReiniciarSensoresDisparados()
		{
			SensoresDisparados = 0;
		}
	}  