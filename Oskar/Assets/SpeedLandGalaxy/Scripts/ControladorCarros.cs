﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class ControladorCarros : MonoBehaviour {

	public static GameObject Carro;
	public static GameObject[] Jugadores;
	public static GameObject[] JugadoresIA;
	 
	public static void CargarCarro()
	{
		Carro = GenerateTopPosition.Carro;
	} 
 
	public static void CargarCarrosRivales()
	{
		Jugadores = GenerateTopPosition.Jugadores;
		JugadoresIA = GenerateTopPosition.JugadoresIA;
	}

	public static void ActivarCarros()
	{  
		Carro.GetComponent<ControlUsuarioCarro>().enabled = true;
		for (int i = 0;i < JugadoresIA.Length; i++)
		{    
			CarAIControl carroUsuarioControl = JugadoresIA[i].GetComponent<CarAIControl>();
			CarController carroControlador = JugadoresIA[i].GetComponent<CarController>();
			carroUsuarioControl.enabled = true;
			carroControlador.enabled = true;
		} 
		for (int i = 0; i < Jugadores.Length; i++)
		{    
			ControlUsuarioCarro carroUsuarioControl = Jugadores[i].GetComponent<ControlUsuarioCarro>();
			carroUsuarioControl.enabled = true;
		} 
	}

	/// <summary>
	/// Desactivars the carros.
	/// </summary>
	public static void DesactivarCarros()
	{  
		for (int i = 0;i < Jugadores.Length; i++)
		{    
			ControlUsuarioCarro carroUsuarioControl = Jugadores[i].GetComponent<ControlUsuarioCarro>();
			carroUsuarioControl.enabled = false;
		} 
		for (int i = 0;i < JugadoresIA.Length; i++)
		{    
			CarAIControl carroUsuarioControl = JugadoresIA[i].GetComponent<CarAIControl>();
			carroUsuarioControl.enabled = false;
		}
	}
}
