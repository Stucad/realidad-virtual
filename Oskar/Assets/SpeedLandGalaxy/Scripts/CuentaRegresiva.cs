﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car; 

public class CuentaRegresiva : MonoBehaviour {

		public int ConteoActual = 0;
		public float Temporizador =10f;  
		public Text ConteoRegresivo;
		public string MensajeInicio="!Ya¡";
		public GameObject SonidoCuentaRegresiva;
		public GameObject SonidoFinal;
		public GameObject SonidoMusicaDeFondo;   
	  
		public static int conteoActual;
	    public static float temporizador;  
	    public static Text conteoRegresivo;
	    public static string mensajeInicio;
	    public static GameObject sonidoCuentaRegresiva;
	    public static GameObject sonidoFinal;
	    public static GameObject sonidoMusicaDeFondo;  
		private static AudioSource fuenteCuentaRegresiva;
	    private static AudioSource fuenteFinal;
	    private static AudioSource musicaFondo;

	private void Awake()
	{
		conteoActual = ConteoActual;
		temporizador = Temporizador;  
		conteoRegresivo = ConteoRegresivo;
		mensajeInicio = MensajeInicio;
		sonidoCuentaRegresiva = SonidoCuentaRegresiva;
		sonidoFinal = SonidoFinal;
		sonidoMusicaDeFondo = SonidoMusicaDeFondo;
	}
	
	  public static void InicializarCuentaRegresiva()
		{  
			fuenteCuentaRegresiva = sonidoCuentaRegresiva.GetComponent<AudioSource>();
			fuenteFinal = sonidoFinal.GetComponent<AudioSource>();
			musicaFondo = sonidoMusicaDeFondo.GetComponent<AudioSource>();
		}
  
		/// <summary>
		/// Cuentas the regresiva. Cuando empieza el juego y empieza contar al revez en 3 segs
		/// </summary>
		public static void GenerarCuentaRegresiva()
		{
			if (conteoActual != -1) {
				Contar();  
				if (conteoActual == 0) { 
					conteoRegresivo.text = mensajeInicio;  
				} 
			}
			if (conteoActual == -1) {
				conteoRegresivo.text = string.Empty; 
			}
		}

		/// <summary>
		/// Contar this instance.
		/// </summary>
		public static void Contar()
		{ 
			conteoActual = (int)temporizador;  
			string conteo = conteoActual.ToString(); 
			if (conteo != conteoRegresivo.text &&  conteoRegresivo.text != mensajeInicio) {
				conteoRegresivo.text = conteo; 
				switch (conteoActual) { 
				case 0:
					musicaFondo.Play();
					break;
				case 1:  
					conteoRegresivo.color = Color.red;
					fuenteFinal.Play();
					break;
				case 2:  
					fuenteCuentaRegresiva.Play();
					break;
			    case 3:  
					fuenteCuentaRegresiva.Play();
					break; 
				}
			}
		    temporizador = temporizador - Time.deltaTime;
		} 
	
	} 