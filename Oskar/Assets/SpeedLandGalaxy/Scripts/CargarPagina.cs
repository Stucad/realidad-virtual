﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
 
public class CargarPagina : MonoBehaviour {

		public float loadingTime;
		public Image loadingBar;
		public Text porcentaje;
		public Text informacion;
		public Text informacion2;
 
		void Awake () {
			informacion2.text = string.Empty;
		/*	int informacion3 = (int) Random.Range (0.0f, 7.0f)  ; 
			switch (informacion3)
			{
			case 0:
				informacion2.text = "Respeta las señales de tránsito.";  
				break;
			case 1:
				informacion2.text = "La vida es sagrada."; 
				break; 
			case 2:
				informacion2.text = "Conductor, ponete, el cinturón de seguridad.";
				break;
			case 3:
				informacion2.text = "Maneje con cuidado."; 
				break; 
			case 4:
				informacion2.text = "Respete al peatón."; 
				break;
			case 5:
				informacion2.text = "Mijos, prudencia con ese carro o esa moto."; 
				break; 
			case 6:
				informacion2.text = "Los circuitos de carreras déjaselo a los profesionales, tú respeta las normas conduzca con cuidado."; 
				break;
			case 7:
				informacion2.text = "Ser conciente de respetar las vidas te hace precavido"; 
				break; 
			} */
		} 

		// Use this for initialization
		void Start () {
			int informacion3 = (int) Random.Range (0.0f, 7.0f)  ; 
			switch (informacion3)
			{
			case 0:
				informacion2.text = "Respeta las señales de tránsito.";  
				break;
			case 1:
				informacion2.text = "La vida es sagrada."; 
				break; 
			case 2:
				informacion2.text = "Conductor, ponete, el cinturón de seguridad.";
				break;
			case 3:
				informacion2.text = "Maneje con cuidado."; 
				break; 
			case 4:
				informacion2.text = "Respete al peatón."; 
				break;
			case 5:
				informacion2.text = "Mijos, prudencia con ese carro o esa moto."; 
				break; 
			case 6:
				informacion2.text = "Los circuitos de carreras déjaselo a los profesionales, tú respeta las normas conduzca con cuidado."; 
				break;
			case 7:
				informacion2.text = "Ser conciente de respetar las vidas te hace precavido"; 
				break; 
			}
			loadingBar.fillAmount = 0;
		}
 
		public void Jugar()
		{
			StartCoroutine (CargarEscenaJuego());
		}

		// Update is called once per frame
		void Update () {
			
		/*	if (loadingBar.fillAmount <= 1) {
				loadingBar.fillAmount += 1.0f / loadingTime * Time.deltaTime;
			}
			if (loadingBar.fillAmount == 1.0f) {
				
			} 
			porcentaje.text = (loadingBar.fillAmount * 100).ToString ("f0");*/
			Jugar ();
		}


		IEnumerator CargarEscenaJuego()
		{
			AsyncOperation resultado = SceneManager.LoadSceneAsync (2);
			resultado.allowSceneActivation = false;

			while (!resultado.isDone) {//si ya finalizo la operacion
				float progreso = Mathf.Clamp01 (resultado.progress / 0.9f);
				loadingBar.fillAmount = progreso;
				porcentaje.text = (progreso * 100).ToString ();

				if (resultado.progress == 0.9f) {
					informacion.text = "Presione una tecla para jugar";
					if (Input.anyKey) {
						resultado.allowSceneActivation = true;
					}
				}
				yield return null;
			}

			yield return resultado;
		}
	} 