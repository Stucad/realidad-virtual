﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;  

public class Velocimetro : MonoBehaviour {

	public float minimaVelocidad = 0.0f;
	public float maximaVelocidad = 0.0f;   
	private static GameObject Carro;
	private static Image imgVelocimetro;
	private static float maximaVelocidad1;

	private void Awake () {  
		maximaVelocidad1 = maximaVelocidad;
		imgVelocimetro = this.GetComponent<Image>();  
	}

	public static void IniciarlizarVelocimetro()
	{
		Carro = ControladorCarros.Carro;  
		maximaVelocidad1 = Carro.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>().MaxSpeed;
	}

	private void Update () { 
		if (Carro != null) {
			float VelocidadActual = Mathf.Round(Carro.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>().CurrentSpeed);
			float Matiz = VelocidadActual * 0.005f; 
			Rigidbody rigidbody = Carro.GetComponent<Rigidbody>(); 
			if (Vector3.Dot (rigidbody.velocity, rigidbody.transform.forward) > 0.0f ) { 
				if (VelocidadActual >= 120.0F) {
					if (imgVelocimetro.fillAmount > 0.0f) {
						imgVelocimetro.color = Color.red;
					}
				} else if (VelocidadActual <= 80.0F) {
					imgVelocimetro.color = Color.white;
				} else if(VelocidadActual > 80.0f && VelocidadActual< 120.0f){
					imgVelocimetro.color = new Color (1f, 1f - Matiz, 1f - Matiz, 1f);
				}
				imgVelocimetro.fillAmount = VelocidadActual / maximaVelocidad1;
			} else { 
				imgVelocimetro.fillAmount = 0.0f;
			} 
		} 
	}
}