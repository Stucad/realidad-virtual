﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTopPosition : MonoBehaviour {
 
	public List<GameObject> carList;
	public GameObject generateTopPosition; 
	public GameObject positionList; 
	public static List<GameObject> carListStatic; 
	public static GameObject positionListStatic;
	public static GameObject[] JugadoresIA;
	public static GameObject[] Jugadores;
	public static GameObject Carro;
	private static GameData gamedata;
	private static int topPositionPlayerIA=0;
	private static int topPositionIntiantiateIA=0; 
	public static GameObject generateTopPosition1;
	private int topPosition = 0;
	private static int userCount;

	private void Awake()
	{ 
		topPosition = positionList.transform.childCount;
		generateTopPosition1 = generateTopPosition;
		carListStatic = carList;
		positionListStatic = positionList;
		gamedata = DataController.gameUser;
		userCount = gamedata.gameUser == null ? 1 : gamedata.gameUser.Count;
		int playerIA = topPosition - userCount;
		topPositionPlayerIA = carListStatic.Count - userCount;//Cuantos players q no se instancian 
		topPositionIntiantiateIA = playerIA - topPositionPlayerIA;
		JugadoresIA = new GameObject[topPositionPlayerIA + topPositionIntiantiateIA];
		Jugadores = new GameObject[topPosition - (topPositionPlayerIA + topPositionIntiantiateIA)]; 
	}

	public static void ReadListPlayers()
	{ 
		if (gamedata.gameUser != null) { 
			ActiveTopPositionPlayerIA (); 
			ActiveTopPositionPlayer ();
		} else { 
			GenerateTopPositionGeneral ();
		}
	}

	/// <summary>
	/// Actives the top position player.
	/// </summary>
	private static void ActiveTopPositionPlayer()
	{
			for (int i = 0;  i < gamedata.gameUser.Count; i++) {
				for (int j = 0; j < carListStatic.Count; j++) { 
					if (gamedata.gameUser [i].car == carListStatic [j].name) {
						
						if (!carListStatic [j].activeSelf) {    
							ApplyPositionRotation (carListStatic [j], JugadoresIA.Length + i );//Por el momento en la última posición 
						if (gamedata.gameUser [i].playerOne == true) {
							Carro = carListStatic [j];
						}
							Jugadores [i] = carListStatic [j]; 
						} else {
							GameObject carIntiantiate = (GameObject)Instantiate (carListStatic [j]); 
							carIntiantiate.transform.parent = generateTopPosition1.transform;  
							ApplyPositionRotation (carIntiantiate, JugadoresIA.Length + i );
							Jugadores [j] = carIntiantiate;
							carIntiantiate.name = carIntiantiate.name + j; 
						} 
						j = carListStatic.Count; 
					}
				}
			} 
	}

	/// <summary>
	/// Actives the top position player IA.
	/// </summary>
	private static void ActiveTopPositionPlayerIA()
	{
			if (topPositionIntiantiateIA > 0) {
				for (int i = 0; i < topPositionIntiantiateIA; i++) { 
					float indexF = (float)carListStatic.Count;  
					int indexI = (int)Random.Range (0.0f, indexF); //ojo puede desvordar si no indexF-1  
						GameObject carIntiantiate = (GameObject)Instantiate (carListStatic [indexI]);
						carIntiantiate.transform.parent = generateTopPosition1.transform;    
						TransformIA (carIntiantiate);
						ApplyPositionRotation (carIntiantiate, i); 
						JugadoresIA[i] = carIntiantiate; 
					    carIntiantiate.name = carIntiantiate.name + i; 
				}
			}
			if (topPositionPlayerIA > 0) {  
					for (int i = 0;  i < userCount; i++) {
						for (int j = 0; j <= topPositionPlayerIA; j++) {  
							if (gamedata.gameUser [i].car != carListStatic [j].name) { 
								if (!carListStatic [j].activeSelf) {   
									TransformIA (carListStatic [j]); 
									int k = topPositionIntiantiateIA + j;
									if (j == 0) {
										JugadoresIA [k] = carListStatic [j];
									} else {
										JugadoresIA [k - 1] = carListStatic [j];
									} 
									ApplyPositionRotation (carListStatic [j], topPositionIntiantiateIA + topPositionPlayerIA - 1);  
								} 
							} 
						}
					}   
			    }
	       }


	private static void GenerateTopPositionGeneral()
	{ 
		int position = positionListStatic.transform.childCount;
		int carListCount = carListStatic.Count;
		int positionList = position - carListCount;
		for (int i = 0; i < position; i++) {
			if (i >= (positionList)) {
				if (!carListStatic [i - positionList].activeSelf) {
					if (i < JugadoresIA.Length) {
						TransformIA (carListStatic [i - positionList]); 
						JugadoresIA [i] = carListStatic [i-positionList];
					} else { 
						Jugadores[i - JugadoresIA.Length] = carListStatic [i - positionList];
						Carro = carListStatic [i-positionList];
					}
					ApplyPositionRotation (carListStatic [i-positionList], i); 
				}
			} else {
				float indexF = (float)carListStatic.Count;  
				int indexI = (int)Random.Range (0.0f, indexF);
				GameObject carIntiantiate = (GameObject)Instantiate (carListStatic [indexI]);
				carIntiantiate.transform.parent = generateTopPosition1.transform;    
				TransformIA (carIntiantiate);
				ApplyPositionRotation (carIntiantiate, i); 
				JugadoresIA[i] = carIntiantiate; 
				carIntiantiate.name = carIntiantiate.name + i; 
			}
		}
	}

	public static void ActiveListaCars()
	{
		for (int i = 0; i < generateTopPosition1.transform.childCount; i++) {
			generateTopPosition1.transform.GetChild (i).gameObject.SetActive (true);
		}
	}

	/// <summary>
	/// Applies the position and rotation of a car.
	/// </summary>
	/// <param name="car">Car.</param>
	/// <param name="position">Position.</param>
	private static void ApplyPositionRotation(GameObject car, int position)
	{ 
		//GameObject objet = positionListStatic.gameObject.transform.GetChild (position).gameObject;
		//car.transform.position = objet.transform.localPosition; 
		car.transform.position = positionListStatic.gameObject.transform.GetChild(position).transform.localPosition;
		car.transform.rotation = positionListStatic.gameObject.transform.GetChild(position).transform.localRotation;  
		car.transform.localPosition = positionListStatic.gameObject.transform.GetChild(position).transform.localPosition;
		car.transform.localRotation = positionListStatic.gameObject.transform.GetChild(position).transform.localRotation;  
	    car.GetComponentInChildren<EmitirRayo> ().PosicionJugador = position + 1;
		if(car.tag == "Player") { //No entre la IA 
			car.GetComponentInChildren<EmitirRayo> ().PosicionEnElJuego.text = car.GetComponentInChildren<EmitirRayo> ().PosicionJugador.ToString();
		} 
	}
 
	private static void TransformIA(GameObject car)
	{ 
		car.GetComponent<UnityStandardAssets.Utility.WaypointProgressTracker> ().enabled = true; 
		car.GetComponentInChildren<ControladorEspecialidadesIA> ().enabled = true;
		car.GetComponentInChildren<EmitirRayo> ().PosicionEnElJuego = null; 
		car.tag = "PlayerIA"; 
	} 
}
