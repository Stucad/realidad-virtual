﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorRecolectores : MonoBehaviour {

		public string[] tipoRecolector;
		private GameObject RecolectoresNinos;
	    private GameObject RecolectorPrincipal; 
		private GameObject RecolectorNino;  

		/// <summary>
		/// Activars los recolectores transito. recorre los objetos para volver activar los signos desactivados por el carro
		/// </summary>
		public void ActivarRecolectoresTransito()
		{ 
		    RecolectorPrincipal = VariablesGlobables.recolectablePrincipal;
			for (int i=0;  i < RecolectorPrincipal.gameObject.transform.childCount; i++)//
				{
					RecolectoresNinos = RecolectorPrincipal.gameObject.transform.GetChild(i).gameObject;
				   for (int j=0;  j < RecolectoresNinos.gameObject.transform.childCount; j++)
						{
							RecolectorNino = RecolectoresNinos.gameObject.transform.GetChild(j).gameObject;
							RecolectorNino.SetActive(true);
						}  
				} 
		}
	}