﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
 
[RequireComponent(typeof (CarController))]
public class ControlUsuarioCarro : MonoBehaviour {

		private CarController Carro; 
		  
		private void Awake()
		{ 
			Carro = GetComponent<CarController>();
		} 
		// Update is called once per frame
		void FixedUpdate() 
	{
			// pass the input to the car! 
			float h = Input.GetAxis("Horizontal1");
			//float v = Input.GetAxis("Vertical"); 
			float v = Input.GetAxis("Vertical1");
			#if !MOBILE_INPUT
			float handbrake = Input.GetAxis("Jump");
			Carro.Move(h, v, v, handbrake);
			#else
			Carro.Move(h, v, v, 0f);
			#endif
			
		}
	} 
