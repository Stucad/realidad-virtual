﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
 
public class ControladorSenales : MonoBehaviour {

	public int Indice;
	public string[] Posiciones;
	public int[] Visible;
	public GameObject ImagenesSenales; 
	public float Temporizador = 8f; 

    public static int indice;
	public static string[] posiciones;
    public static int[] visible;
    public static GameObject imagenesSenales; 
	public static float temporizador; 
	public static float temporizadorFijo;  
	private static int conteoActual = 0;
    private static bool empezarCuentaRegresiva = false;

	private void Awake()
	{
		indice = Indice;
		posiciones = Posiciones;
		visible = Visible;
		imagenesSenales = ImagenesSenales; 
		temporizador = Temporizador;  
		temporizadorFijo = temporizador;
	}
  
    public static void MostrarSenal(float posicion)
	{    
		if (indice < posiciones.Length) 
		{ 
			if (posicion >= float.Parse(posiciones[indice].Split(',')[0]) && 
				posicion <= float.Parse(posiciones[indice].Split(',')[1]) && visible[indice] == 0){
				imagenesSenales.gameObject.transform.GetChild(indice).gameObject.SetActive (true); 
				visible [indice] = 1; //que pasa cuando hay mas de un vuelta en el juego?
				empezarCuentaRegresiva = true;
				indice++;
				/*for (int i = 0; i < CantidadRecolectoresTipo.Length; i++) { 
				cantidadRecolectores.gameObject.transform.GetChild(i).GetComponentInChildren<Text>().text = CantidadRecolectoresTipo[i].ToString(); 
			}*/
			}
			if (posicion <= float.Parse(posiciones[indice].Split(',')[0]) && 
				posicion >= float.Parse(posiciones[indice].Split(',')[1]) && visible[indice] == 0){
				imagenesSenales.gameObject.transform.GetChild(indice).gameObject.SetActive (true); 
				visible [indice] = 1; 
				empezarCuentaRegresiva = true;
				indice++; 
			} 
		}
		if (empezarCuentaRegresiva == true) {
			CuentaRegresiva();
		}
	}
 
		/// <summary>
		/// Contar this instance.
		/// </summary>
		public static void CuentaRegresiva()
		{
		 temporizador = temporizador - Time.deltaTime;
		  conteoActual = (int)temporizador;  
			if (conteoActual == 0) {
				imagenesSenales.gameObject.transform.GetChild(indice - 1).gameObject.SetActive (false);
			    temporizador = temporizadorFijo;
			     empezarCuentaRegresiva = false;
			}
		}
  }