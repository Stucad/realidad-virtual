﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 


namespace Assets.Escena2_SenalesDeTransito.Scripts
{ 
	public class SeleccionarImagen : MonoBehaviour {

		public RawImage tipAyudaPrincipal;
		public Button btnTipAyudaEntrada;

		private RawImage tipAyudaMini;

		void Start () { 
			tipAyudaPrincipal.texture = btnTipAyudaEntrada.GetComponent<RawImage>().texture;
		}

		public void PonerImagen(Button btnAyuda)
		{ 
			tipAyudaMini = btnAyuda.GetComponent<RawImage>();
			tipAyudaPrincipal.texture = tipAyudaMini.texture;
		}
	}
}
