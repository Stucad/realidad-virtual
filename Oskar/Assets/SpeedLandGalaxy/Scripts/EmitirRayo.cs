﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using System;  

public class EmitirRayo : MonoBehaviour {

		public int PosicionJugador;
		public Text PosicionEnElJuego;
		private RaycastHit Impactos;
		private GameObject[] JugadoresRivales;
		private GameObject[] JugadoresIARivales;
		private GameObject Carro = null;
		private GameObject PadreJugadorImpacto; 
		private GameObject Impacto;
		private EmitirRayo RayoImpactado; 
		private GameObject posicionesTranscursoJuego;
		private GameObject posicionarNombresFinalJuego;
		private ControladorDeVueltas controladorVueltas;
		private int Mascara; 

		void Awake(){ 
		    bucarCarro(); 
			Mascara = 1 << 8;// Hay que orginizarlo porque se van a quitar algunos layers
		}

		// Use this for initialization
		void Start () 
		    { 
				controladorVueltas = Carro.GetComponentInChildren<ControladorDeVueltas>();
			    JugadoresRivales = ControladorCarros.Jugadores; //devuelve todos los jugadores
			    JugadoresIARivales = ControladorCarros.JugadoresIA; //devuelve todos los jugadores IA
				//posicionesTranscursoJuego = controladorJuego.GetComponent<PosicionarJugadores>().posicionesTranscursoJuego; 
			}

			/// <summary>
			/// Fixeds the update. 
			/// </summary>
		void Update()//si la posicion no es tan precisia cambiar a FixedUpdated
		     {    
				if (JugadoresRivales != null) { 
					CrearEmicionDeRayo(JugadoresRivales);  
				}
				if (JugadoresIARivales != null) {
					CrearEmicionDeRayo(JugadoresIARivales); 
	             }   
			}

		/// <summary>
		/// Crears the emicion de rayo.
		/// </summary>
		/// <param name="JugadoresRivalesEncontrados">Jugadores encontrados.</param>
		private void CrearEmicionDeRayo(GameObject[] JugadoresRivalesEncontrados)
			{  
				for (int i = 0; i < JugadoresRivalesEncontrados.Length; i++) {  
					if (Carro.transform.name != JugadoresRivalesEncontrados [i].transform.name) { //Ojo esta validación no es 100% exacta: debe interceptar a todos los rivales excepto al jugador principal
						Vector3 DireccionDelRayoEmitir = JugadoresRivalesEncontrados [i].transform.position - this.transform.position;
					//Debug.DrawRay (transform.position, DireccionDelRayoEmitir, Color.red);
						if (Physics.Raycast (transform.position, DireccionDelRayoEmitir, out Impactos, 20f, Mascara, QueryTriggerInteraction.Collide)) {//Crea el rayo 
							PosicionarJugador (Impactos, JugadoresRivalesEncontrados [i]);
						}  
					}
				  }
			}

			private void PosicionarJugador(RaycastHit ImpactosEncontrados, GameObject JugadorRival)
			{
				GameObject CarroImpactando;  
				if(ImpactosEncontrados.collider.gameObject.tag == "ForwardPlayer")//Detecta que el rayo emitido esta impactando los sensores significa que esta adelante 
				{ 
			        CarroImpactando = ImpactosEncontrados.collider.gameObject.transform.GetComponentInParent<ControladorDeVueltas>().gameObject; 
					if (Carro.transform.name == CarroImpactando.transform.name) {// Pregunta si tu carro es el que esta impactando y no otros o mejor dicho si tu rayo es el que impacta
						AsignarPosicion(JugadorRival);
					}
				}  
			} 

		void AsignarPosicion(GameObject JugadorRival)
		{
			GameObject CarroImpactado;  
		    CarroImpactado = JugadorRival.transform.GetComponentInParent<ControladorDeVueltas>().gameObject;//Captura el carro que impacto 
		    RayoImpactado = CarroImpactado.transform.GetComponentInChildren<EmitirRayo>(); 
			ControladorDeVueltas controladorVueltasCarroImpactado = CarroImpactado.GetComponentInChildren<ControladorDeVueltas>();
			int VueltasLlevadasCarroImpactado = controladorVueltasCarroImpactado.VueltasLlevadas;
			int VueltasLlevadas = controladorVueltas.VueltasLlevadas;  
			if (VueltasLlevadas == VueltasLlevadasCarroImpactado) // Para pasar al otro tiene que estar en igual de vueltas
			{ 
				int SensoresDisparadosCarroImpactado = controladorVueltasCarroImpactado.SensoresDisparados;
				int SensoresDisparadosCarro = controladorVueltas.SensoresDisparados;
				bool SuperoPosicion = false; 
				if (SensoresDisparadosCarroImpactado >= SensoresDisparadosCarro) {
					SuperoPosicion = true;
				}   
				if(SuperoPosicion == true)
				{  
					int posicion = RayoImpactado.PosicionJugador; //Recuerda tienes que verlo tanto si es el jugador
					if (posicion > PosicionJugador) {//Si la posicion del jugador principal lo sobrepasa
						//OrdenarPosicion(PosicionJugador, JugadorRival.name);
						//OrdenarPosicion(posicion, Carro.name);
						if (Carro.tag == "PlayerIA" && JugadorRival.tag == "Player") { //No entra la IA 
							RayoImpactado.PosicionJugador = PosicionJugador;
							RayoImpactado.PosicionEnElJuego.text = PosicionJugador.ToString (); //Rayo impactado del jugador player
							PosicionJugador = posicion; 
						} else if (Carro.tag == "Player" && JugadorRival.tag == "PlayerIA") {  
							RayoImpactado.PosicionJugador = PosicionJugador;
							PosicionEnElJuego.text = posicion.ToString (); //Rayo impactado del jugador player
							PosicionJugador = posicion; 
						}else if (Carro.tag == "Player" && JugadorRival.tag == "Player") { 
							RayoImpactado.PosicionJugador = PosicionJugador;
							RayoImpactado.PosicionEnElJuego.text = PosicionJugador.ToString (); //Rayo impactado del jugador player
							PosicionJugador = posicion;
						}
						else if (Carro.tag == "PlayerIA" && JugadorRival.tag == "PlayerIA") { 
							RayoImpactado.PosicionJugador = PosicionJugador; 
							PosicionJugador = posicion;
						}
					} 
				} 
			}    
		} 

		void OrdenarPosicion(int posicion, string nombre)
		{ 
			//posicionesTranscursoJuego.gameObject.transform.GetChild(posicion - 1).GetComponentInChildren<Text>().text = nombre;    
		}

		/// <summary>
		/// Bucars the padre del turbo.
		/// </summary>
		/// <returns>The padre del turbo.</returns>
	private void bucarCarro()
		{ 
			var t = transform; 
			while (Carro == null)
				{
					string player = t.tag;
					switch(player)
					{
						case  "Player":
					       Carro = t.gameObject; 
							break;
						case  "PlayerIA":
					        Carro = t.gameObject; 
							break;
						default:
					     t = t.parent; 
							break; 
					} 
			  } 
		}
	}