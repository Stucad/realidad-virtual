﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorTiempo : MonoBehaviour {
 
	public static string tiempoJuegoActual;  
	private static float startTime; 
 
	/// <summary>
	/// Tiempo this instance. Tiempo en el juego
	/// </summary>
	public static void ArrancarConometro()
	{  
		startTime = VariablesGlobables.startTime;
		float tiempoJuego = Time.time - startTime; 
		var tiempoJuego2 = Mathf.Max(0, tiempoJuego - Time.deltaTime);   
		var time = System.TimeSpan.FromSeconds(tiempoJuego2); 
		tiempoJuegoActual = string.Format("{0:00}:{1:00}:{2:000}", time.Minutes, time.Seconds, time.Milliseconds); 
	} 
}
