﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MostrarSenalACapturar : MonoBehaviour {

	public List<GameObject> senalList; 
	public Text capture;
	public static List<GameObject> senalList1; 
	public static Text capture1;
	private static int index;

	void Awake()
	{   
		senalList1 = senalList;
		capture1 = capture;
	} 

	public static void MostrarSenalTipo()
	{
		capture1.text = "Capturar la señal:";
		float indexF = (float)senalList1.Count;  
		 index = (int)Random.Range (0.0f, indexF);
		senalList1[index].SetActive(true);
		capture1.text += senalList1[index].name;
	}

	public static void OcultarSenalTipo()
	{
		capture1.text = ""; 
		senalList1[index].SetActive(false); 
	}
}
