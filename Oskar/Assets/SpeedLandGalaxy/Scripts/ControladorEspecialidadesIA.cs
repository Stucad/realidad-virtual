﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorEspecialidadesIA : MonoBehaviour {

	public int indice = 0;
	public string[] posiciones;
	private bool[] sensoresVueltas;  

	void Update(){  
		float posicion = this.gameObject.transform.position.x;
		ConsultarPosicion(posicion);
	}
 
	public void ConsultarPosicion(float posicion)
	{    
		sensoresVueltas = this.gameObject.GetComponent<ControladorDeVueltas>().sensoresVueltas; 
		if (indice < posiciones.Length) 
		{  
			if (sensoresVueltas.Length > 0) {
				if (posicion >= float.Parse(posiciones[indice].Split(',')[0]) && 
					posicion <= float.Parse(posiciones[indice].Split(',')[1]) && sensoresVueltas[indice] == true){
					ActivarEspecialidad ();
					indice++;

				}
				if (posicion <= float.Parse(posiciones[indice].Split(',')[0]) && 
					posicion >= float.Parse(posiciones[indice].Split(',')[1]) && sensoresVueltas[indice] == true){
					ActivarEspecialidad ();
					indice++; 
				}
			} 
		}
	}

	public void ActivarEspecialidad()
	{
		int especialidad = (int)Random.Range (0.0f, 2.0f); 
		switch (especialidad) {
		case 1:
			this.gameObject.GetComponent<Turbo> ().activarTurbo = true;
				break;
		default:
			if (indice == posiciones.Length) { 
				float range = (float)(posiciones.Length - 1);
				indice =  (int)Random.Range (0.0f, range); 
			    }
			break;
		}
	}
}
