﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class VariablesGlobables : MonoBehaviour {

	public GameObject Sensores;
	public GameObject RecolectablePrincipal; 
	public GameObject Camara;
	public GameObject[] RecolectoresSecundarios;
	public int NumeroVueltas;

	public static int numeroVueltas;
	public static bool pararTiempo = false;
	public static GameObject recolectablePrincipal; 
	public static GameObject camara;
	public static GameObject[] recolectoresSecundarios; 
	public static GameObject sensores;
	public static float startTime; 

	private void Awake()
	{
		startTime = Time.time + 3;
		recolectablePrincipal = RecolectablePrincipal; 
		camara = Camara;
		recolectoresSecundarios = RecolectoresSecundarios;
		sensores = Sensores;
		numeroVueltas = NumeroVueltas;
	}
}  