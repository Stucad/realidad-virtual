﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;
using System;

public class IndicadoresDelJuego : MonoBehaviour {

	public Text Tiempo;
	public Text Vueltas; 
	public Text Velocidad;
	public Text PuestoFinal;
	public Text PremioPorPuesto; 
	public string[] Premios; 
	public GameObject CantidadRecolectoresCapturados; 
	public GameObject PosicionesTiemposFinalJuego;
	public GameObject PosicionarNombresFinalJuego; 
	public GameObject CantidadPorTipoActual; 
	public GameObject CantidadPorTipoFinal; 
	public GameObject TiposDeSenalesTransitoActual;

	public Action<Text> prueba;
	public static Text tiempo;
	public static Text vueltas; 
	public static Text velocidad;
	public static Text puestoFinal;
	public static Text premioPorPuesto; 
	public static string[] premios; 
	public static GameObject cantidadRecolectoresCapturados; 
	public static GameObject posicionesTiemposFinalJuego;
	public static GameObject posicionarNombresFinalJuego; 
	public static GameObject cantidadPorTipoActual; 
	public static GameObject cantidadPorTipoFinal; 
	public static GameObject tiposDeSenalesTransitoActual;
	private static GameObject Carro;
	//private CarController carController;
	private static Animator animarFinalJuego;

	private void Awake()
	{    
		 tiempo= Tiempo;
		 vueltas= Vueltas ; 
		 velocidad= Velocidad;
		 puestoFinal= PuestoFinal;
		 premioPorPuesto= PremioPorPuesto; 
		 premios= Premios; 
		 cantidadRecolectoresCapturados= CantidadRecolectoresCapturados; 
		 posicionesTiemposFinalJuego= PosicionesTiemposFinalJuego;
		 posicionarNombresFinalJuego= PosicionarNombresFinalJuego; 
		 cantidadPorTipoActual= CantidadPorTipoActual; 
		 cantidadPorTipoFinal= CantidadPorTipoFinal; 
		 tiposDeSenalesTransitoActual= TiposDeSenalesTransitoActual;
		 animarFinalJuego= gameObject.transform.root.gameObject.GetComponent<Animator>(); 
	}
 
	public static void EntregarTiempo()
	{
		tiempo.text = ControladorTiempo.tiempoJuegoActual;
	}

	public static void EntregarIndicadoresActuales()
	{ 
		vueltas.text= "" + ControladorCarros.Carro.GetComponentInChildren<ControladorDeVueltas>().VueltasLlevadas + "/" + VariablesGlobables.numeroVueltas;
		velocidad.text= Mathf.Round(ControladorCarros.Carro.GetComponent<CarController>().CurrentSpeed) + " KPH";  	 
	} 

	public static void EntregarIndicadoresFinales(int posicion, int[] CantidadRecolectoresTipo, string Jugador, string nombre)
	{
		if (Jugador == "Player") {
			VariablesGlobables.pararTiempo = true; 
			animarFinalJuego.SetBool("FinalJuego", true);
			premioPorPuesto.text = premios[posicion - 1];
			RecorrerRecolectoresPorTipo(CantidadRecolectoresTipo);
			RecorrerRecolectoresPorSimbolo();
		}  
		puestoFinal.text = posicion.ToString();
		premioPorPuesto.text = premios[int.Parse(puestoFinal.text) - 1]; 
		posicionarNombresFinalJuego.gameObject.transform.GetChild(posicion - 1).GetComponentInChildren<Text>().text = nombre;
		posicionesTiemposFinalJuego.gameObject.transform.GetChild(posicion - 1).GetComponentInChildren<Text>().text = ControladorTiempo.tiempoJuegoActual; 

	}
 
	/// <summary>
	/// Recorrers the recolectores por tipo. Recorre todos los recolectores contados y se lo asigna a la imagen
	/// </summary>
	/// <param name="CantidadRecolectoresTipo">Cantidad recolectores tipo.</param>
	public static void RecorrerRecolectoresPorTipo(int[] CantidadRecolectoresTipo)
	{ 
		for (int i = 0; i < CantidadRecolectoresTipo.Length; i++) { 
			cantidadRecolectoresCapturados.gameObject.transform.GetChild(i).GetComponentInChildren<Text>().text = CantidadRecolectoresTipo[i].ToString(); 
		}
	}

	public static void RecorrerRecolectoresPorSimbolo()
	{  
	    for (int j = 0; j < cantidadPorTipoActual.transform.childCount; j++) { 
			cantidadPorTipoFinal.gameObject.transform.GetChild(j).GetComponentInChildren<Text>().text = cantidadPorTipoActual.gameObject.transform.GetChild(j).GetComponentInChildren<Text>().text; 
		} 
	}

	public void PararAnimacionFinalJuego()
	{
		animarFinalJuego.enabled = false;
	}
}