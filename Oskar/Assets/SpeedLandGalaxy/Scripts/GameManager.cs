﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car; 
using UnityEngine.SceneManagement;
 
public class GameManager : MonoBehaviour { 
     
	  private void Start()
	  {  
		GenerateTopPosition.ReadListPlayers();
		ControladorCarros.CargarCarro();
		ControladorCarros.CargarCarrosRivales();
		GenerateTopPosition.ActiveListaCars();
		IndicadoresMovimiento.IniciarIndicadores();  
		IndicadorTurbo.IniciarIndicadorTurbo();
		Velocimetro.IniciarlizarVelocimetro ();
		CuentaRegresiva.InicializarCuentaRegresiva();//Esto hay que mejorarlo
		MostrarSenalACapturar.MostrarSenalTipo();
		IndicadoresDelJuego.EntregarIndicadoresActuales();//analizar esto 
	  }

		private void Update() 
		{ 
		  if (ControladorCarros.Carro != null) { 
				  CuentaRegresiva.GenerarCuentaRegresiva();
					switch(CuentaRegresiva.conteoActual)
					{
						case 0:
							ControladorTiempo.ArrancarConometro(); 
							ControladorCarros.ActivarCarros();
							break;
			             case -1:
							ControladorTiempo.ArrancarConometro ();  
							MostrarSenalACapturar.OcultarSenalTipo ();
							break; 
					}
					if (VariablesGlobables.pararTiempo == false) { 
						IndicadoresDelJuego.EntregarTiempo (); 
					}  
				IndicadoresDelJuego.EntregarIndicadoresActuales();
			    ControladorSenales.MostrarSenal(ControladorCarros.Carro.transform.position.x);// muestra señales de transito cuando pasa por x posición
			}
		} 
   
		/// <summary>
		/// Siguientes the nivel. Carga a la siguiente escena
		/// </summary>
		/// <param name="nivel">Nivel.</param>
		public void SiguienteNivel(int nivel)
		{
		  SceneManager.LoadScene(nivel); 
		} 
} 