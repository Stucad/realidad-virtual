﻿using UnityEngine;
using System.Collections; 
using System.Collections.Generic;
using UnityEngine.UI;

public class SelectorDeCoches : MonoBehaviour {

		public List<GameObject> carList;
		public int index = 0;
		public Text player; 

		private void Awake() 
		{   
		   carList[index].SetActive(true); 
		}

		private void Start()
		{
			SelectPlayer();
		    SelectNameCar(carList[index].name);
		}

		public void Next()
		{
			carList[index].SetActive(false);
			if (index == carList.Count - 1) {
				index = 0;
			} else {
				index++; 
			} 
			carList[index].SetActive (true);
		    SelectNameCar (carList [index].name); 
		}

		public void Previous()
		{
			carList[index].SetActive (false);
			if (index == 0) {
				index = carList.Count - 1;
			} else {
				index--; 
			} 
			carList[index].SetActive (true); 
		   SelectNameCar (carList [index].name); 
		}

		private void SelectPlayer()
	    {  
			GameData gameUser= DataController.gameUser;
			player.text= gameUser.gameUser[0].user;
		}

		private void SelectNameCar(string name)
	    { 
			GameData gameUser= DataController.gameUser;
			gameUser.gameUser[0].car = name;
		}

		private void Update () 
		{
			carList[index].transform.Rotate (0, 0.15f, 0); 
		}  
} 