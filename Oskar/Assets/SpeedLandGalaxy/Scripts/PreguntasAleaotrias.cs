﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Assets.Scripts
{ 
public class PreguntasAleaotrias : MonoBehaviour {

	public GameObject pnlFinal; 
	public GameObject pnlPreguntasRespuestas;
	public GameObject imgPuntosPorPregunta; 
	public Text pregunta;
    public Text respuestaIncorrecta;

	private int? respuesta = 0; 
	private string[] puntos;


		/// <summary>
		/// Crea una pregunta aleatoria
		/// </summary>
    void Start () { 
		respuesta = (int)Random.Range (1f, 12f);  
		switch (respuesta)
		{
		case 1:
			pregunta.text = "¿La señales preventivas a que figura pertenece?"; 
			this.gameObject.transform.Find("pnlListaRespuesta").gameObject.SetActive(true);
			break;
		case 2:
			pregunta.text = "¿La señales informativas a que figura pertenece?";
			this.gameObject.transform.Find("pnlListaRespuesta").gameObject.SetActive(true);
			break;
		case 3:
			pregunta.text = "¿La señales reglamentarias a que figura pertenece?";
			this.gameObject.transform.Find("pnlListaRespuesta").gameObject.SetActive(true);
			break;
		case 4:
			pregunta.text = "¿Las señales de fondo azul y símbolos negros son?";
			this.gameObject.transform.Find("pnlListaRespuesta2").gameObject.SetActive(true);
			break;
		case 5:
			pregunta.text = "Las señales que proporcionan información, " +
				"sobre los servicios y lugares que nos vamos a encontrar" +
				" en el camino pertenecen a:";
			this.gameObject.transform.Find("pnlListaRespuesta2").gameObject.SetActive(true);
			break;
		case 6:
			pregunta.text = "Señal que por lo regular es cuadrada:";
			this.gameObject.transform.Find("pnlListaRespuesta").gameObject.SetActive(true);
			break;
		case 7:
			pregunta.text = "¿Las señales de fondo blanco con borde rojo y letra negra son?";
			this.gameObject.transform.Find("pnlListaRespuesta2").gameObject.SetActive(true);
			break;
		case 8:
			pregunta.text = "Las señales que tienen como objetivo mostrar los límites y " +
				"prohibiciones que hay en el camino; su violación constituye una falta que puede ser sancionable:";
			this.gameObject.transform.Find("pnlListaRespuesta2").gameObject.SetActive(true);
			break;
		case 9:
			pregunta.text = "Señal que por lo regular es redonda:";
			this.gameObject.transform.Find("pnlListaRespuesta").gameObject.SetActive(true);
			break; 
		case 10: 
			pregunta.text = "Estas señales son de color amarillo con símbolos y letras negras:";
			this.gameObject.transform.Find("pnlListaRespuesta2").gameObject.SetActive(true);
			break;
		case 11:
			pregunta.text = "La función de estas señales es mostrar " +
				"al conductor los riesgos o factores de atención existentes en el recorrido:";
			this.gameObject.transform.Find("pnlListaRespuesta2").gameObject.SetActive(true);
			break;
		case 12:
			pregunta.text = "¿Señal que por lo regular es un rombo?:";
			this.gameObject.transform.Find("pnlListaRespuesta").gameObject.SetActive(true);
			break; 
		} 
	}

		/// <summary>
		/// Busca la respueta por el respectivo simbolo del boton
		/// </summary>
		/// <param name="btnFigura">Button figura.</param>
	public void Respuesta(Button btnFigura)
	{ 
		switch (respuesta)
		{
			case 1:
				if (btnFigura.name == "btnRombo") { 
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 2:
				if (btnFigura.name == "btnCuadrado") { 
					LimpiarRespuestaIncorrecta();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 3:
				if (btnFigura.name == "btnCirculo") { 
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 4:
				if (btnFigura.name == "btnCuadrado") { 
					pnlFinal.SetActive(true);
					LimpiarRespuestaIncorrecta();
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 5: 
				if (btnFigura.name == "btnCuadrado") {
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive(true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
				} 
				break;
			case 6: 
				if (btnFigura.name == "btnCuadrado") {
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive(true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 7:
				if (btnFigura.name == "btnCirculo") {
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar ();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 8:
				if (btnFigura.name == "btnCirculo") {
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 9:
				if (btnFigura.name == "btnCirculo") {
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 10:
				if (btnFigura.name == "btnRombo") {
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break; 
			case 11: 
				if (btnFigura.name == "btnRombo") {
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
			case 12:
				if (btnFigura.name == "btnRombo") {
					LimpiarRespuestaIncorrecta ();
					pnlFinal.SetActive (true);
					ObtenerPuntos(btnFigura.name);
					Ocultar();
				} else {
					MostrarRespuestaIncorrecta();
				} 
				break;
		}
	}

		void ObtenerPuntos(string figura) 
		{
			int[] puntaje;
			switch (figura) {
			case "btnRombo":
				puntaje = new int[]{ 2, 1, 1 };
				MostrarPuntos(puntaje);
				break;
			case "btnCirculo":
				puntaje = new int[]{1, 2, 1};
				MostrarPuntos(puntaje);
				break;
			case "btnCuadrado":
				puntaje = new int[]{1, 1, 2};
				MostrarPuntos(puntaje);
				break;
			}
		}

		void MostrarPuntos(int[] puntaje)
		{ 
			for (int i=0;  i < imgPuntosPorPregunta.gameObject.transform.childCount; i++)//
			{
				imgPuntosPorPregunta.gameObject.transform.GetChild(i).GetComponent<Text>().text = puntaje[i].ToString(); 
			} 
		}
 
		void MostrarRespuestaIncorrecta()
		{
			respuestaIncorrecta.text = "Respuesta incorrecta.";
		}

		void LimpiarRespuestaIncorrecta()
		{
			respuestaIncorrecta.text = string.Empty;
		}

		 void Ocultar()
		{ 
			pnlPreguntasRespuestas.SetActive (false);
		}
}
}
