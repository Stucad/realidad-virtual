﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
 
public class Recolectores : MonoBehaviour {

	public GameObject explosionRecoletable;
	private CapturarRecolectores capturarRecolectores; 
		// Update is called once per frame
		void Update () {
			transform.Rotate(new Vector3(30, 20, 10) * Time.deltaTime);
		} 

		void OnTriggerEnter(Collider collider)
		{  
		    if (collider.name == "Sensor")
			{  
			    Instantiate (explosionRecoletable, transform.position, transform.rotation);
			     capturarRecolectores = collider.transform.GetComponentInParent<CapturarRecolectores>(); 
				capturarRecolectores.IdenficarGruposRecolectores(this.gameObject); 
			}  	 
		}

		void OnTriggerExit(Collider collider)
		{   
		   if (collider.name == "Sensor") { 
			    capturarRecolectores = collider.transform.GetComponentInParent<CapturarRecolectores>(); 
				capturarRecolectores.AsignarGruposRecolectores (this.gameObject);
			}
		}
} 