﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

namespace Assets.Escena1_PistaEnElAire.ControladorJuego.Scripts
{
	public class SeleccionImagenRecolector : MonoBehaviour {

		public RawImage imagenCentral;
		public Button btnRecolector; 

		void Start () { 
			imagenCentral.texture = btnRecolector.transform.GetChild(0).GetComponent<RawImage>().texture;
		}

		public void PonerImagen(Button btnRecolector)
		{ 
			imagenCentral.texture = btnRecolector.transform.GetChild(0).GetComponent<RawImage>().texture;
		}
	}
}