﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class DataController : MonoBehaviour {

	public GameData gameUserPublic  = new GameData(); 
	public static GameData gameUser = new GameData();
	private string filePath ;
	private static string jsonString; 
	private string gameDataProjectFilePath = "/SpeedLandGalaxy/Data/gameData.json";
	// Update is called once per frame
	private void Awake () {
		ReadData ();
		gameUserPublic = gameUser;
	}

	private void Start()
	{
		DontDestroyOnLoad (gameObject);
	}

	public void ReadPath()
	{
		filePath = Application.dataPath + gameDataProjectFilePath;
	}

	public void ReadData()
	{
		ReadPath ();
		if (File.Exists (filePath)) { 
			jsonString = File.ReadAllText (filePath);
			GameData gameU = JsonUtility.FromJson<GameData> (jsonString);
			gameUser.gameUser = gameU.gameUser; 
		}
	}

	public void SaveData ()
	{ 
		jsonString = JsonUtility.ToJson(gameUser);
		ReadPath ();
		File.WriteAllText (filePath, jsonString);
	}
}
