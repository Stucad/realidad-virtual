﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
 
public class EnfoqueMenu : MonoBehaviour {

		public float VelocidadMovimientoCamara = 3.0f;
		private Transform CamaraTransform;
		private Transform CamaraEnfoque;

		void Start()
		{
			CamaraTransform = Camera.main.transform;
		}

		void Update () {
			if (CamaraEnfoque != null) {
				CamaraTransform.rotation = Quaternion.Slerp (CamaraTransform.rotation, CamaraEnfoque.rotation, VelocidadMovimientoCamara * Time.deltaTime);
			}
		}


		public void EnfocarCamara(Transform MenuAEnfocar)
		{ 
			CamaraEnfoque = MenuAEnfocar; 
		}
} 