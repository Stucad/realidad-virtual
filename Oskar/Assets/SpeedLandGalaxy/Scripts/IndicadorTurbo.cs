﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class IndicadorTurbo : MonoBehaviour {

	public Text textTemporizador;
	public Text textTiempoActivo;
	public Text textCantidadTurbo; 
	private Image imgIndicadorTurbo; 
	private bool activarTurbo;
	private static float temporizador;
	private static int temporizadorInicial; 
	private static Text textTemporizador1;
	private static Text textTiempoActivo1;
	private static Text textCantidadTurbo1;
	private static Image imgIndicadorTurbo1;
	private static Turbo turbo;
	private static Image imgTurbo;

	private void Awake () { 
		textTemporizador1 = textTemporizador;
		textTiempoActivo1 = textTiempoActivo;
		textCantidadTurbo1 = textCantidadTurbo;
		imgIndicadorTurbo1 = imgIndicadorTurbo;
		imgTurbo = this.GetComponent<Image>(); 
	}

	public static void IniciarIndicadorTurbo () {  
		turbo = ControladorCarros.Carro.GetComponent<Turbo>();
		imgIndicadorTurbo1 = imgTurbo; 
		temporizador = turbo.temporizador;  
		textCantidadTurbo1.text = turbo.cantidadTurbo.ToString();
		temporizadorInicial = (int)temporizador;
		textTiempoActivo1.text = temporizadorInicial.ToString() + "Seg."; 
	}

	/// <summary>
	///Activa el turbo
	/// </summary>
	private void Update () {
		if (turbo != null) {
				activarTurbo = turbo.activarTurbo;  
				float temporizadorActual = turbo.temporizador; 
				if (activarTurbo == true) {
					if (temporizador == temporizadorActual) {//Compara si el temporizador empieza en el tiempo que se ha configurado
						temporizadorInicial = (int)temporizador;
						textTemporizador1.text = temporizadorInicial.ToString();
						imgIndicadorTurbo1.fillAmount = 1.0f;//Incia en 1
					} else {
						imgIndicadorTurbo1.fillAmount = temporizadorActual/temporizador;
						temporizadorInicial = (int)temporizadorActual;
						textTemporizador1.text = temporizadorInicial.ToString();
					} 
				}
				if(	activarTurbo == false   ) {
					imgIndicadorTurbo1.fillAmount = 0.0f;
					if (temporizadorActual == 0.0f) {
						textTemporizador1.text = string.Empty; 
					} 
				} 
				textCantidadTurbo1.text = turbo.cantidadTurbo.ToString();
		} 
	}
} 
