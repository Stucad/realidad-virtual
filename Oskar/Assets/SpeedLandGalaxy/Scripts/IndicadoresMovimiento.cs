using System.Collections;
using System.Collections.Generic;
using UnityEngine;

  public class IndicadoresMovimiento : MonoBehaviour {

		public GameObject indicadorMovimientoPrincipal;
		public GameObject indicadorMovimientoSecundario;  
	    public GameObject indicadores;  
		private static GameObject indicadorMovimientoPrincipal1;
		private static GameObject indicadorMovimientoSecundario1;
	    private static GameObject indicadores1;   
		private static GameObject[,] indicadorMovimientoSecundarios;
		private static GameObject[,] indicadorMovimientoSecundariosIA;
		private static GameObject carro;
		private static GameObject[] jugadoresRivales;
		private static GameObject[] jugadoresIARivales;
		private static GameObject controladorJuego;
		private static bool indicadorSecundarioAsignado = false;
		private static float positionY;

		// Use this for initialization
		void Awake () 
	    {  
			indicadorMovimientoPrincipal1 = indicadorMovimientoPrincipal;
			indicadorMovimientoSecundario1 = indicadorMovimientoSecundario;
		    indicadores1 = indicadores;
			positionY = transform.position.y; 
		}

		public static void IniciarIndicadores()
		{
			jugadoresRivales =ControladorCarros.Jugadores; //devuelve todos los jugadores
			jugadoresIARivales = ControladorCarros.JugadoresIA; //devuel 
			carro = ControladorCarros.Carro;
			indicadorMovimientoPrincipal1.transform.position = new Vector3 (carro.transform.position.x, positionY, carro.transform.position.z); 
			indicadorMovimientoSecundarios = new GameObject[jugadoresRivales.Length,2];
			indicadorMovimientoSecundariosIA = new GameObject[jugadoresIARivales.Length,2];
			if (jugadoresRivales.Length > 0) {
				CrearIndicadoresSecundarios(jugadoresRivales);
			} 
			if (jugadoresIARivales.Length > 0) {
				CrearIndicadoresSecundariosIA(jugadoresIARivales);
			} 
		}
 
		// Update is called once per frame
		void Update () 
	    {
			if (carro != null) { 
				SeguirMovimientoCarroPrincipal();
			}
			if (jugadoresRivales != null) {
				SeguirMovimientoCarrosSecundarios(jugadoresRivales);
			} 
		    if (jugadoresIARivales != null) {
				SeguirMovimientoCarrosSecundariosIA(jugadoresIARivales); 
			} 
		}
 
		/// <summary>
		/// Crears the indicadores secundarios. crea los indicadores de los jugadores
		/// </summary>
		/// <param name="jugadores">Jugadores.</param>
		private static void CrearIndicadoresSecundarios(GameObject[] jugadores)
		{ 
			if (jugadores [0].name != carro.name) { //Como ya existe el indicador a medir se le asigna al primero
				indicadorSecundarioAsignado = true;
				indicadorMovimientoSecundario1.transform.position =  new Vector3 (jugadores[0].transform.position.x, positionY, jugadores[0].transform.position.z);
				indicadorMovimientoSecundarios [0, 0] = indicadorMovimientoSecundario1;
				indicadorMovimientoSecundarios [0, 1] = jugadores[0];
			}
			if (jugadores.Length > 1) { // a los demas se le crea una instancia del indicador primero 
				for (int i = 1; i < jugadores.Length; i++) {
					if (jugadores [i].name != carro.name) {
						GameObject indicadorSecundario = Instantiate (indicadorMovimientoSecundario1);
					     indicadorSecundario.transform.parent = indicadores1.transform;
						indicadorSecundario.transform.position = new Vector3 (jugadores [i].transform.position.x, positionY, jugadores [i].transform.position.z); 
					    indicadorMovimientoSecundarios [i, 0] = indicadorSecundario;
						indicadorMovimientoSecundarios [i, 1] = jugadores [i];
					}
				}
			}  
		}

		/// <summary>
		/// Crears the indicadores secundarios I.Crea los indicadores de movimiento a la IA
		/// </summary>
		/// <param name="jugadoresIA">Jugadores I.</param>
		private static void CrearIndicadoresSecundariosIA(GameObject[] jugadoresIA)
		{  
			if ( indicadorSecundarioAsignado == false) { 
				indicadorSecundarioAsignado = true; 
				indicadorMovimientoSecundario1.transform.position =  new Vector3 (jugadoresIA[0].transform.position.x, positionY, jugadoresIA[0].transform.position.z);
				indicadorMovimientoSecundariosIA [0, 0] = indicadorMovimientoSecundario1;
				indicadorMovimientoSecundariosIA [0, 1] = jugadoresIA[0];
			}
			if (jugadoresIA.Length > 1) {
				for (int i = indicadorSecundarioAsignado == false? 0 : 1; i < jugadoresIA.Length; i++) {
					if (jugadoresIA [i].name != carro.name) {
						GameObject indicadorSecundario = Instantiate (indicadorMovimientoSecundario1);
					    indicadorSecundario.transform.parent = indicadores1.transform;
						indicadorSecundario.transform.position = new Vector3 (jugadoresIA [i].transform.position.x, positionY, jugadoresIA [i].transform.position.z); 
					indicadorMovimientoSecundariosIA [i, 0] = indicadorSecundario;
						indicadorMovimientoSecundariosIA [i, 1] = jugadoresIA [i];
					}
				}
			} 
		}

		/// <summary>
		/// Sigue el movimiento carro principal.
		/// </summary>
		void SeguirMovimientoCarroPrincipal()
		{
			indicadorMovimientoPrincipal.transform.position = new Vector3 (carro.transform.position.x, transform.position.y, carro.transform.position.z);
		}

		/// <summary>
		/// sigue el movimiento carros secundarios.
		/// </summary>
		/// <param name="jugadores">Jugadores.</param>
		void SeguirMovimientoCarrosSecundarios(GameObject[] jugadores)
		{  
			if (indicadorMovimientoSecundarios.GetLength(0) > 1) {
				for (int i = 0; i < indicadorMovimientoSecundarios.GetLength(0); i++) {
					indicadorMovimientoSecundarios[i, 0].transform.position = new Vector3 (indicadorMovimientoSecundarios [i, 1].transform.position.x, transform.position.y, indicadorMovimientoSecundarios [i, 1].transform.position.z);
				}
			}
		}

		/// <summary>
		/// Sigue el movimiento carros secundarios IA.
		/// </summary>
		/// <param name="jugadoresIA">Jugadores I.</param>
		void SeguirMovimientoCarrosSecundariosIA(GameObject[] jugadoresIA)
		{  
			for (int i = 0; i < indicadorMovimientoSecundariosIA.GetLength(0); i++) {
				indicadorMovimientoSecundariosIA[i, 0].transform.position = new Vector3 (indicadorMovimientoSecundariosIA[i, 1].transform.position.x, transform.position.y, indicadorMovimientoSecundariosIA[i, 1].transform.position.z);
			}
		}
	}
