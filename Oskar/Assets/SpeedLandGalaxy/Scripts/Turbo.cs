﻿using System.Collections; 
using UnityEngine;
using UnityStandardAssets.Effects;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Vehicles.Car; 
using System;  

public class Turbo : MonoBehaviour {

		public Color[] minimosColoresParticulas;
		public bool activarTurbo = false;
		public float temporizador;
		public int cantidadTurbo = 1;
		public float aumentarPotencia = 10.0f; 
		public GameObject turbo;
		public GameObject AudioTurbo; 
		private ParticleSystem[] particulasSistemas; 
		private float[] tamanosOriginalComienzo = null;  
		private float[] vidasOriginalParticula  = null; 
		private Color[] ColoresOriginalesParticulas  = null; 
		//private CarController Carro;  	// Ojo con carro y carcontroller hay que unirlo
		private CarController carController;
		private float temporizadorGuardado; 
		private WheelCollider[] ruedasColisionadoras;// busca la ruedas que van estar en el piso 
		//private float CampoDeVision = 0f;
		private float Desdibujar;
		private AudioSource ActivarFuente = null; 
	    private static GameObject Camara;

		/// <summary>
		/// Awake this instance.
		/// </summary>
		void Awake(){  
			carController = bucarPadreDelTurbo();
			ruedasColisionadoras = carController.transform.GetComponentsInChildren<WheelCollider>(); 
		}

		/// <summary>
		/// Start this instance.
		/// </summary>
		private void Start () {  
		if (this.tag == "Player") {
			ConfigurarCamaraPlayer();
		} 
			ActivarFuente = AudioTurbo.GetComponent<AudioSource>();
			//CampoDeVision = Camera.main.fieldOfView;  
			temporizadorGuardado = temporizador;
			//carController = Carro.GetComponent<UnityStandardAssets.Vehicles.Car.CarController>();
			particulasSistemas = turbo.GetComponentsInChildren<ParticleSystem>();
			vidasOriginalParticula = new float[particulasSistemas.Length];
			tamanosOriginalComienzo= new float[particulasSistemas.Length];
			ColoresOriginalesParticulas = new Color[particulasSistemas.Length];
			for (int i = 0; i < particulasSistemas.Length; i++) {
				vidasOriginalParticula[i] = particulasSistemas[i].main.startLifetime.constant;
				tamanosOriginalComienzo[i] = particulasSistemas[i].main.startSize.constant;
				ColoresOriginalesParticulas[i] = particulasSistemas[i].main.startColor.color;
			}
		}

		private void ConfigurarCamaraPlayer()
		{
			Camara = VariablesGlobables.camara; 
		}

		 
		/// <summary>
		/// Update this instance.
		/// Activa el turbo temporalmente
	    // cuando se mueve objetos con la fisica se utiliza fixedupdate
		/// </summary>
		private void FixedUpdate () { 
		if (this.tag == "Player") {
			AnalizarTurboPlayer();
			} else {
			AnalizarTurboPlayerIA();
			}
		}

		private void AnalizarTurboPlayer()
		{
			if (Input.GetButton("Fire1") && cantidadTurbo > 0 && carController.BrakeInput == 0.0f ) { 
				activarTurbo = true;//sirve para indicar en la imagen cuanto turbo hay 
				ActivarTurbo();
			}
			else { 
				activarTurbo = false;
				DesactivandoTurbo();
			}
			EfectoCamaraVelocidad();
		}

		private void AnalizarTurboPlayerIA()
		{
		if (activarTurbo == true && temporizador > 0) {//sirve para activar el turbo
				ActivarTurbo ();
			} else {
				DesactivandoTurbo();
			}
		}

		private void ActivarTurbo()
		{
			AumentoPotencia();  
			if (!ActivarFuente.isPlaying) { 
				ActivarFuente.Play(); 
			}  
			temporizador = temporizador - Time.deltaTime;
			TurboCar(1.0f); 
			if ((int) temporizador == 0) { 
				cantidadTurbo = cantidadTurbo - 1;
				if (cantidadTurbo > 0) {
					temporizador = temporizadorGuardado;
				} else {
					temporizador = 0.0f;
				} 
			} 
		}

		private void DesactivandoTurbo()
		{
			TurboCar(0.0f);
			if (ActivarFuente.isPlaying) { 
				ActivarFuente.Stop(); 
			} 
		}

		/// <summary>
		/// Aumentos the potencia.
		/// </summary>
		private void AumentoPotencia()
		{
			foreach( WheelCollider ruedaColision in ruedasColisionadoras)
			{
				if(ruedaColision.isGrounded == true)//Agrega potencia cuando esta en el suelo
				{ 
				    carController.transform.GetComponent<Rigidbody>().AddForce (transform.forward * aumentarPotencia, ForceMode.Acceleration);
				}
			}
		}

		/// <summary>
		/// Efectos the camara velocidad.
		/// </summary>
		private void EfectoCamaraVelocidad()
		{  
			MotionBlur Desenfoque = Camara.GetComponent<MotionBlur>();  
		    float VelocidadActual = carController.CurrentSpeed,  ReducirDesenfoque = 0.002f, Aceleracion = carController.GetComponent<Rigidbody>().velocity.magnitude;
			if (VelocidadActual > 120.0f) {
				ReducirDesenfoque = 0.010f;
				Desenfoque.blurAmount = VelocidadActual > 280.0f ? 0.25f : (Aceleracion * 0.005f) - ReducirDesenfoque;  
			} else {
				Desenfoque.blurAmount = 0f;
			}
			/*if (Camera.main.fieldOfView <= 60.0f) {
				Camera.main.fieldOfView = ( VelocidadActual*0.5f)  > CampoDeVision? VelocidadActual*0.5f : CampoDeVision;
			} */ 
		}

		/// <summary>
		/// Turbos the car.
		/// </summary>
		/// <param name="turbo">Turbo.</param>
		public void TurboCar(float turbo)
		{
			for (int i = 0; i < particulasSistemas.Length; i++) { 
				ParticleSystem.MainModule moduloPrincipal = particulasSistemas[i].main;
				moduloPrincipal.startLifetime = Mathf.Lerp(0.0f, vidasOriginalParticula[i],turbo);
				moduloPrincipal.startSize = Mathf.Lerp(tamanosOriginalComienzo[i]*.3f, tamanosOriginalComienzo[i], turbo);
				moduloPrincipal.startColor = Color.Lerp(minimosColoresParticulas[i], ColoresOriginalesParticulas[i], turbo);
			}
		}

		/// <summary>
		/// Bucars the padre del turbo.
		/// </summary>
		/// <returns>The padre del turbo.</returns>
		private CarController bucarPadreDelTurbo()
		{ 
			var t = transform; 
			while (t != null)
			{
				var turbo = t.GetComponent<CarController>();
				if (turbo == null)
				{ 
					t = t.parent;
				}
				else
				{
					return turbo;
				}
			} 
			throw new Exception(" No funciona controlador");
		} 	
	}  