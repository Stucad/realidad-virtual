﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Escena2_SenalesDeTransito.Scripts
{ 
	public class Salir : MonoBehaviour {
		
		public void Quit()
		{
			Application.Quit();
		}

		public void Jugar(int Nivel)
		{
			SceneManager.LoadScene(Nivel); 
		}
	}
}
