﻿using UnityEngine;
using System.Collections; 

namespace UnityStandardAssets.Vehicles.Car
{
	public class RayoPlasma : MonoBehaviour {


		//private GameObject colisionadorInferior;
		//private GameObject Energy;
		//public Rigidbody Energy2;
		//private GameObject SalidaEnergia;
		public AudioClip comienzoSonido, finalSonido; 

		private GameObject ActivarRayo; 
		private ParticleSystem rayoPlasmas; 
		private int cantidadParticulas = 0;
		private AudioSource source;
		private bool soundBoleano = false;


		// Use this for initialization
		void Start () {
			//colisionadorInferior = GameObject.Find("ColisionadorCarroceria");
			//Energy = GameObject.Find("RayoPlasma"); 
			//SalidaEnergia = GameObject.Find("SalidaEnergia");
			ActivarRayo = GameObject.Find("LaserVerde");
			rayoPlasmas = ActivarRayo.GetComponent<ParticleSystem> (); 
			source = gameObject.AddComponent<AudioSource>(); 
		}

		// Update is called once per frame
		void Update () {
			if (Input.GetMouseButton (0)) {

				//rayoPlasmas = ActivarRayo.GetComponent<ParticleSystem> (); 
			 

				rayoPlasmas.Simulate (0f);
				rayoPlasmas.Play ();
				//Debug.Log ("emitiendo: " + rayoPlasmas.isEmitting);
		 
				//int numeroParticulas = rayoPlasmas.particleCount;
				//Debug.Log ("particulas: " + numeroParticulas);
			
				//	Rigidbody nuevaEnergia = (Rigidbody) Instantiate(Energy2, SalidaEnergia.transform.position, colisionadorInferior.transform.rotation);
			
				//	nuevaEnergia.velocity = transform.TransformDirection(Vector3.forward * 200f); 
				soundBoleano = true;
				 
			} else {
 
				if (rayoPlasmas.isEmitting && soundBoleano == true) {
					soundBoleano = false;
					Debug.Log ("entro rayo emitiendo ");
					if (!source.isPlaying) {
						Debug.Log ("entro play " + " " + source.isPlaying);

						source.clip = comienzoSonido;
						source.volume = 4;
						source.Play (); 
						source.maxDistance = 15;
					} 
				} else {
					Debug.Log ("entro rayo no emitiendo ");
					if (!rayoPlasmas.isEmitting) {
						if (source.isPlaying) {
							Debug.Log ("entro play stop " + " " + source.isPlaying);
							source.minDistance = 0;
							source.volume = 0;
							source.Pause ();
							source.Stop ();
						}
					}
				}
 
				//Debug.Log ("emitiendo: " + rayoPlasmas.isEmitting);

				//int numeroParticulas = rayoPlasmas.particleCount;
				///Debug.Log ("particulas: " + numeroParticulas);
				//AudioSource source = GetComponent<AudioSource>();

				//if (numeroParticulas > cantidadParticulas) {
					//Debug.Log ("Sonido uno: " );
					
					//source.time = Random.Range (0, comienzoSonido.length);
				
					//source.loop = true;


				//} else {
					//Debug.Log ("Sonido dos numero de particulas  "  + numeroParticulas);
					

			
					//source.clip = finalSonido;
				//	source.volume = 10;
					//source.loop = true;
					//source.Play ();
				//	source.Stop ();
				//}//
			}

		}
	}
}
